import React from "react";
import "./modal.scss"
export class Modal extends React.Component {
       
    render(){
        return(
            <div className= {this.props.bakGround}  onClick={()=>{this.props.onClose()}}>
            <div className= {this.props.wrapper} onClick={this.props.wrapperClose}>
                <h1 className= {this.props.headerText} >{this.props.header}</h1>
               {this.props.closeButton && <div onClick={()=>{
                    this.props.onClose()
                }} className= {this.props.btvCloth}/>}
                <p className= {this.props.bodyText}>{this.props.text}</p>
                <div >{this.props.action}</div>
            </div>
            </div>
            
         
        )
    }
}

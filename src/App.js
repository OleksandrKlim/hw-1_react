import React from "react";

import { Modal } from "./component/Modal/Modal";
import { Button } from "./component/Button/Button";
import "./App.scss";

class App extends React.Component {
  state = {
    isModalVisibleFirst: false,
    isModalVisibleSecond: false,
  };
  render() {
    return (
      <div className="App">
        <Button
          class="btn"
          backgroundColor="#69C798"
          text="Open first modal"
          handleClick={() => {
            this.setState({ ...this.state, isModalVisibleFirst: true });
          }}
        />
        <Button
          class="btn"
          backgroundColor="#A1BED0"
          text="Open second modal"
          handleClick={() => {
            this.setState({ ...this.state, isModalVisibleSecond: true });
          }}
        />

        {this.state.isModalVisibleFirst && (
          <Modal
            bakGround="wrapper"
            wrapper="modal"
            headerText="modal__header"
            btvCloth="modal__cloth"
            bodyText="modal__bodyText"
            closeButton={true}
            header="Do you want to delete this file?"
            text="Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?"
            wrapperClose={(e) => e.stopPropagation()}
            onClose={() => {
              this.setState({ ...this.state, isModalVisibleFirst: false });
            }}
            action={
              <>
                <Button
                  class="modal__btn"
                  text="Ok"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      isModalVisibleFirst: false,
                    });
                  }}
                />
                <Button
                  class="modal__btn"
                  text="Cancel"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      isModalVisibleFirst: false,
                    });
                  }}
                />
              </>
            }
          />
        )}
        {this.state.isModalVisibleSecond && (
          <Modal
            bakGround="wrapper"
            wrapper="modal"
            headerText="modal__header"
            btvCloth="modal__cloth"
            bodyText="modal__bodyText"
            closeButton={true}
            text="Press ok to delete this file, cancel if you change your mind"
            header="Are you sure you want to delete this file?"
            wrapperClose={(e) => e.stopPropagation()}
            onClose={() => {
              this.setState({ ...this.state, isModalVisibleSecond: false });
            }}
            action={
              <>
                <Button
                  class="modal__btn"
                  text="Ok"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      isModalVisibleSecond: false,
                    });
                  }}
                />
                <Button
                  class="modal__btn"
                  text="Cancel"
                  handleClick={() => {
                    this.setState({
                      ...this.state,
                      isModalVisibleSecond: false,
                    });
                  }}
                />
              </>
            }
          />
        )}
      </div>
    );
  }
}

export default App;
